module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    concat: {
      options: {
        separator: ';'
      },
      dist: {
        src: [
			'www/ui/js/libs/jquery-2.0.0.js',
			'www/ui/js/libs/jquery-ui.min.js',
			'www/ui/js/libs/jquery.jsonp-2.3.0.js',
			'www/ui/js/libs/app.js',
			'www/ui/js/libs/app.localstorage.js',
			'www/ui/js/libs/fastClick.js'
		],
        dest: 'www/ui/js/app.min.js'
      }
    },
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
      },
      dist: {
        files: {
          'www/ui/js/app.min.js': ['<%= concat.dist.dest %>'] // dist/<%= pkg.name %>.min.js
        }
      }
    },
//    qunit: {
//      files: ['test/**/*.html']
//    },
    jshint: {
      files: ['www/ui/js/libs/app.js', 'www/test/**/*.js'],
      options: {
        // options here to override JSHint defaults
        globals: {
          jQuery: true,
          console: true,
          module: true,
          document: true
        }
      }
    },
    watch: {
      files: ['<%= concat.dist.src %>', 'www/ui/sass/*.scss'],
      tasks: ['concat', 'compass:dev'] //  'qunit', 
    },
    compass: { // Task
      dist: {
	      options: {
	        sassDir: 'www/ui/sass',
      	    cssDir: 'www/ui/css',
	        outputStyle: 'compressed'
	      }
      },
      dev: { // Another target
      	options: {
      	  sassDir: 'www/ui/sass',
      	  cssDir: 'www/ui/css',
      	  outputStyle: 'expanded'
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib');
  grunt.loadNpmTasks('grunt-contrib-compass');

  // grunt.registerTask('test', ['jshint' ]); //'qunit'

  grunt.registerTask('default', ['concat', 'uglify', 'compass:dist']); //'qunit', 

};
